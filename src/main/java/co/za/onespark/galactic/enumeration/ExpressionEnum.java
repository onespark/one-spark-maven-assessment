package co.za.onespark.galactic.enumeration;

public enum ExpressionEnum {
    GLOB("glob", "I", 1),
    PROK("prok", "V", 5),
    PISH("pish", "X", 10),
    TEGJ("tegj", "L", 250);
    String expression;
    String romanNumeral;
    Integer number;

    ExpressionEnum(String expression, String romanNumeral, Integer number) {
        this.expression = expression;
        this.romanNumeral = romanNumeral;
        this.number = number;
    }

    public String getRomanNumeral() {
        return this.romanNumeral;
    }

    public String getExpression() {
        return this.expression;
    }

    public Integer getNumber() {
        return this.number;
    }
}
