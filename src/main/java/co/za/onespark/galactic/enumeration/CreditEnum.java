package co.za.onespark.galactic.enumeration;

public enum CreditEnum {
    SILVER("Silver",3.4),
    GOLD("Gold", 5780),
    IRON("Iron", 195.5);
    String creditName;
    double value;

    CreditEnum(String creditName, double value) {
        this.creditName = creditName;
        this.value = value;
    }

    public String getCreditName() {
        return this.creditName;
    }

    public double getValue() {
        return this.value;
    }
}
