package co.za.onespark.galactic.exception;

public class OneSparkException extends Exception {

    public OneSparkException(String message) {
        super(message);
    }
}
