package co.za.onespark.galactic;

import co.za.onespark.galactic.constant.Constants;
import co.za.onespark.galactic.enumeration.CreditEnum;
import co.za.onespark.galactic.enumeration.ExpressionEnum;
import co.za.onespark.galactic.exception.OneSparkException;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class OneSparkGalacticApplication {

    public static void main(String[] args) throws OneSparkException {
        Constants.populateRomanNumeralMap();
        String expression = JOptionPane.showInputDialog("Please enter expression");
        Matcher questionMatcher = getMatcher(expression);
        boolean creditsMatcher = expression.contains("Credits");
        if (questionMatcher.matches()) {
            String galacticNumerals = questionMatcher.group(1).trim();
            List<String> galacticNumeralList = Arrays.asList(galacticNumerals.split(" "));
            String romanNumeral = galacticNumeralList.stream().map(c -> ExpressionEnum.valueOf(c.toUpperCase()).getRomanNumeral()).collect(Collectors.joining());
            int result = getResult(romanNumeral);

            System.out.println(result);
        } else if (creditsMatcher) {
            List<String> galacticNumeralList = Arrays.asList(expression.substring(expression.indexOf("is") + 2, getToIndex(expression)).trim().split(" "));

            int result = 0;
            if (getCreditName(expression).equalsIgnoreCase(CreditEnum.SILVER.getCreditName())
                    || getCreditName(expression).equalsIgnoreCase(CreditEnum.GOLD.getCreditName())) {
                result = (int) ((5 + 5) * (CreditEnum.valueOf(getCreditName(expression).toUpperCase()).getValue()));
            } else if (getCreditName(expression).equalsIgnoreCase(CreditEnum.IRON.getCreditName())) {
                result = (int) ((10 + 10) * (CreditEnum.valueOf(getCreditName(expression).toUpperCase()).getValue()));
            }

            System.out.println(getGalacticNumbers(galacticNumeralList) + " " + getCreditName(expression) + " is " + result + " Credits");
        } else {
            String message = "I have no idea what you are talking about";
            System.out.println(message);
            throw new OneSparkException(message);
        }
    }

    /**
     * This method takes in a list of galactic numerals
     * then it appends the galactic numerals and returns them to the caller
     *
     * @param galacticNumeralList
     *  this is the galactic numeral list
     * @return
     *  appended galactic numerals
     */
    public static String getGalacticNumbers(@NotNull List<String> galacticNumeralList) {
        StringBuilder galacticNumbers = new StringBuilder();
        for (String galacticNumber: galacticNumeralList) {
            galacticNumbers.append(" ").append(galacticNumber);
        }
        return galacticNumbers.toString();
    }

    /**
     * This method takes in the expression as a parameter and checks if there is a credit name in the expression
     * then return the index of the credit name
     * if there is no credit name in the expression then the method throws a custom exception
     *
     * @param expression
     *  this is the expression parameter
     * @return
     *  index of the credit
     * @throws OneSparkException
     *  custom exception thrown when no credit is found in the expression
     */
    public static int getToIndex(@NotNull String expression) throws OneSparkException {
        int index;
        if (expression.lastIndexOf(Constants.SILVER) != -1) {
            index = expression.lastIndexOf(Constants.SILVER);
        } else if (expression.lastIndexOf(Constants.GOLD) != -1) {
            index = expression.lastIndexOf(Constants.GOLD);
        } else {
            index = expression.lastIndexOf(Constants.IRON);
        }

        if (index == -1) {
            throw new OneSparkException("Expression does not contain any credit type");
        }

        return index;
    }

    /**
     * This method takes in the expression and checks if there is a credit name in the expression
     * then return the credit name
     * if there is no credit name in the expression then the method throws a custom exception
     *
     * @param expression
     *  this is the expression parameter
     * @return
     *  return the credit name
     * @throws OneSparkException
     *  throws the custom exception when there is no credit name found in the expression
     */
    public static String getCreditName(@NotNull String expression) throws OneSparkException {
        String creditName = "";
        if (expression.lastIndexOf(Constants.SILVER) != -1) {
            creditName = Constants.SILVER;
        } else if (expression.lastIndexOf(Constants.GOLD) != -1) {
            creditName = Constants.GOLD;
        } else if (expression.lastIndexOf(Constants.IRON) != -1) {
            creditName = Constants.IRON;
        }

        if (creditName.trim().equalsIgnoreCase("")) {
            throw new OneSparkException("Expression does not contain any credit type");
        }

        return creditName;
    }

    public static Matcher getMatcher(@NotNull String line){
        return Pattern.compile(Constants.QUESTION_PATTERN).matcher(line);
    }


    /**
     * This method takes in a roman numeral as a parameter and return the calculated numeric value
     *
     * @param romanNumeral
     *  this is the roman numeral parameter
     * @return value
     */
    public static int getResult(@NotNull String romanNumeral) {
        int value = 0;
        Matcher matcherSymbol = Constants.ROMAN_NUMERAL_PATTERN.matcher(romanNumeral);
        while (matcherSymbol.find()) {
            value = Constants.ROMAN_NUMERAL_MAP.entrySet().stream().filter(map -> map.getKey().equals(matcherSymbol.group(0)))
                    .map(Map.Entry::getValue).reduce(value, Integer::sum);
        }
        return value;
    }
}