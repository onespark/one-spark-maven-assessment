package co.za.onespark.galactic.constant;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Constants {
    public static final String QUESTION_PATTERN = "^\\s*how\\s+much\\s+is\\s+([a-z\\s]+)\\s+\\?\\s*$";
    public static final Map<String, Integer> ROMAN_NUMERAL_MAP = new HashMap<>();
    public static final Pattern ROMAN_NUMERAL_PATTERN = Pattern.compile("M|CM|D|CD|C|XC|L|XL|X|IX|V|IV|I");
    public static final String SILVER = "Silver";
    public static final String GOLD = "Gold";
    public static final String IRON = "Iron";

    public static void populateRomanNumeralMap() {
        ROMAN_NUMERAL_MAP.put("I", 1);
        ROMAN_NUMERAL_MAP.put("IV", 4);
        ROMAN_NUMERAL_MAP.put("V", 5);
        ROMAN_NUMERAL_MAP.put("IX", 9);
        ROMAN_NUMERAL_MAP.put("X", 10);
        ROMAN_NUMERAL_MAP.put("XL", 40);
        ROMAN_NUMERAL_MAP.put("L", 50);
        ROMAN_NUMERAL_MAP.put("XC", 90);
        ROMAN_NUMERAL_MAP.put("C", 100);
        ROMAN_NUMERAL_MAP.put("CD", 400);
        ROMAN_NUMERAL_MAP.put("D", 500);
        ROMAN_NUMERAL_MAP.put("CM", 900);
        ROMAN_NUMERAL_MAP.put("M", 1000);
    }
}
