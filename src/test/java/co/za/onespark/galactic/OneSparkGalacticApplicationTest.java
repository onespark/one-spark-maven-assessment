package co.za.onespark.galactic;

import co.za.onespark.galactic.constant.Constants;
import co.za.onespark.galactic.exception.OneSparkException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OneSparkGalacticApplicationTest {


    @Test
    void getGalacticNumbersTest() {
        List<String> galacticNumberList = new ArrayList<>();
        galacticNumberList.add("glob");
        galacticNumberList.add("prok");
        galacticNumberList.add("pish");
        galacticNumberList.add("tegj");

        String galacticNumbers = OneSparkGalacticApplication.getGalacticNumbers(galacticNumberList);

        Assert.assertNotNull(galacticNumbers);
    }

    @Test
    void getToIndexTest() throws OneSparkException {
        String expression = "how many Credits is glob prok Silver ?";

        int index = OneSparkGalacticApplication.getToIndex(expression);

        Assert.assertEquals(30, index);
    }

    @Test
    void getCreditNameTest() throws OneSparkException {
        String expression = "how many Credits is glob prok Silver ?";

        String creditName = OneSparkGalacticApplication.getCreditName(expression);

        Assert.assertEquals(Constants.SILVER, creditName);
    }

    @Test
    void getResultTest() {
        Constants.populateRomanNumeralMap();
        String romanNumeral = "IV";

        int result = OneSparkGalacticApplication.getResult(romanNumeral);

        Assert.assertEquals(4, result);
    }
}
